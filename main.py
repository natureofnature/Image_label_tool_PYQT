import sys
from PyQt5.QtGui import QPixmap, QFont
from PyQt5.QtWidgets import QApplication,QSplashScreen
from PyQt5.QtCore import Qt
from Labeling_tool import Window
import time


def main():
    EXIT_CODE_REBOOT = -15123123 # you can use any unique value here
    exit_code = EXIT_CODE_REBOOT 
    counter = 0
    app = QApplication([str(counter)])
    counter = counter + 1
    splash = QSplashScreen(QPixmap("./configure_files/splash.jpg"))
    splash.show()
    font = QFont()
    font.setPointSize(30)
    font.setBold(True)
    font.setWeight(75)
    splash.setFont(font)
    time.sleep(0.5)
    splash.showMessage("Labeling_tool by LWZ ",Qt.AlignCenter,Qt.yellow)
    time.sleep(1)
    info = "Please wait"
    for i in range(4):
        splash.showMessage(info, Qt.AlignCenter, Qt.yellow)
        time.sleep(0.25)
        info = info+"."
    
    time.sleep(1)

    while exit_code == EXIT_CODE_REBOOT:
        app.processEvents()
        scr = app.primaryScreen()
        screen = Window(scr)
        screen.show()
        splash.finish(screen)
        exit_code = app.exec_()


if __name__ == '__main__':
    main()


