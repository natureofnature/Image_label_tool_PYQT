from PIL import Image
import os
import sys
import numpy as np
import cv2




def floodfill(image_in,image_out):
    mask_image = image_in 
    im_in = cv2.imread(mask_image,0)
    n = 100
    th, im_th = cv2.threshold(im_in, n, 255, cv2.THRESH_BINARY);

    # Copy the thresholded image.
    im_floodfill = im_th.copy()

    # Mask used to flood filling.
    # Notice the size needs to be 2 pixels than the image.
    h, w = im_th.shape[:2]
    mask = np.zeros((h + 2, w + 2), np.uint8)

    # Floodfill from point (0, 0)
    cv2.floodFill(im_floodfill, mask, (0, 0), 255);

    # Invert floodfilled image
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)

    # Combine the two images to get the foreground.
    fill_image = im_th | im_floodfill_inv

    cv2.imwrite(image_out,fill_image)

def main():
    folder = sys.argv[1]
    save_to = sys.argv[2]
    names = os.listdir(folder)
    for name in names:
        image_path = os.path.join(folder,name)
        image_to_path = os.path.join(save_to,name)
        floodfill(image_path,image_to_path)


    






if __name__ == '__main__':
    main()
