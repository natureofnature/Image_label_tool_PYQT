'''
    Copyright (C) <2019>  <natureofnature>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

'''
from PyQt5.QtCore import *
from PIL import Image,ImageDraw
from PyQt5 import QtCore,QtGui
from PyQt5.QtGui import QImage, QPainter, QPalette, QPixmap
from Configure import getConfig,setConfig,getLastDialogue,setPath
import imghdr
import threading
import os
from PyQt5.QtWidgets import *
import sys
import time
from shutil import copy, move
from PyQt5.Qt import QCursor
from global_variable import get_screen_size,get_mouse_press_position
from Configure import getConfig,setConfig,getLastDialogue,setPath,getLabelDic
from global_variable import set_screen_size,set_mouse_press_position
import cv2


class my_QLineEdit(QLineEdit):
    def __init__(self):
        super(my_QLineEdit,self).__init__()
    def focusOutEvent(event):
        super().focusOutEvent(event)


class my_QScrollArea(QScrollArea):
    def __init__(self,widget=None):
        super(my_QScrollArea,self).__init__()
        self.registered_widget = widget 
    
    def get_size(self):
        return self.width(),self.height()

    def resizeEvent(self,event):
        super().resizeEvent(event)
        if self.registered_widget is not None:
            self.registered_widget.resize(self.width(),self.height())

    def wheelEvent(self,event):
        #do nothing
        pass

class my_QLabel(QLabel):

    DRAWING_MODE="PAINT" #or "ERASE"

    
    def __init__(self):
        super(my_QLabel,self).__init__()

    def set_pen_width(self,value):
        pass

    def setNumClasses(self,value):
        pass

    def set_bbx_color(self,value):
        pass

    def set_ruler_color(self,value):
        pass

    def set_paint_mode(self,mode = "PAINT"):
        self.DRAWING_MODE = mode

    def round_coord(self):
        pass

    def set_qimage(self,qimage):
        pass

    def clear_labels(self):
        pass

    def clear_all(self):
        pass

    def get_wheel_ratio(self):
        return None

    def setImageScale(self,scale):
        pass

    def undo(self):
        pass

    def get_bboxes(self):
        return None

    def previous(self):
        pass
    
    def get_qimage(self):
        return None

    def is_labeled(self):
        return False


class my_QLabel_rectangle(my_QLabel):
    def __init__(self,update_text_key):
        super(my_QLabel_rectangle,self).__init__()
        self.setStyleSheet('QFrame {background-color:white;}')
        self.update_text_key = update_text_key
        self.coord = [0,0,0,0]
        self.history_all = []
        self.coord_list = []
        self.position_lists = []
        self.label_lists = [] #store labels
        self.window_status = []
        config_dic,_ = getConfig()
        self.bbx_color = config_dic['bbx_color']
        self.ruler_color=config_dic['ruler_color']
        self.color_dic={'green':QtCore.Qt.green,'red':QtCore.Qt.red,'yellow':QtCore.Qt.yellow,'white':QtCore.Qt.white,'blue':QtCore.Qt.blue,'black':QtCore.Qt.black}
        self.penRectangle = QtGui.QPen(self.color_dic[self.bbx_color])
        self.penRectangle_ruler = QtGui.QPen(self.color_dic[self.ruler_color],1,QtCore.Qt.DashDotLine)
        self.penRectangle.setWidth(1)
        self.released = True
        self.image_scale = 1
        self.label_dic = getLabelDic()
        self.num_class = config_dic['number_classes']
        self.wheel_x = 0
        self.wheel_y = 0
        self.x = 0
        self.y = 0
        self.setMouseTracking(True)

    def set_pen_width(self,value):
        pass
    def setNumClasses(self,value):
        self.num_class= value
        
    def set_bbx_color(self,value):
        self.bbx_color = value
        self.penRectangle = QtGui.QPen(self.color_dic[self.bbx_color])
    def set_ruler_color(self,value):
        self.ruler_color = value
        self.penRectangle_ruler = QtGui.QPen(self.color_dic[self.ruler_color],1,QtCore.Qt.DashDotLine)


    #def wheelEvent(self,event):
    #    super().wheelEvent(event)
    #    delta = event.angleDelta()
    #   print(delta)

    def round_coord(self):
        for i in range(len(self.coord)):
            self.coord[i] = int(self.coord[i])

    def set_qimage(self,qimage):
        pass

    def paintEvent(self, event):
        super().paintEvent(event)
        qp = QtGui.QPainter(self)
        br = QtGui.QBrush(QtGui.QColor(100, 10, 10, 40))  
        #qp.setBrush(br)   
        if self.released is False:
            qp.setPen(self.penRectangle)
            qp.drawRect(QtCore.QRect(self.coord[0],self.coord[1],self.coord[2]-self.coord[0],self.coord[3]-self.coord[1]))
        qp.setPen(self.penRectangle_ruler)
        qp.drawLine(int(self.coord[2]),0,int(self.coord[2]),20000)
        qp.drawLine(0,int(self.coord[3]),20000,int(self.coord[3]))

        index = 0
        for coord in self.coord_list:
            qp.setPen(self.penRectangle)
            qp.drawRect(QtCore.QRect(int(coord[0]),int(coord[1]),int(coord[2]-coord[0]),int(coord[3]-coord[1])))
            if index < len(self.label_lists):
                label = self.label_lists[index]
                label_tag = "undefined"
                if str(label) in self.label_dic:
                    label_tag = self.label_dic[str(label)]
                qp.drawText(int(coord[0])-5,int(coord[1])-5,label_tag)
                index = index + 1



    def clear_labels(self):
        
        del(self.coord_list[:])
        del(self.position_lists[:])
        del(self.label_lists[:])
        self.update()

    def clear_all(self):
        del(self.coord_list[:])
        del(self.position_lists[:])
        del(self.label_lists[:])
        del(self.history_all[:])
    def get_wheel_ratio(self):
        width,height = self.pixmap().width(),self.pixmap().height()
        return self.wheel_x/width,self.wheel_y/height



    def mousePressEvent(self, event):
        if len(self.label_lists) != len(self.coord_list):#no label is set
            print(len(self.label_lists))
            return
        
        self.coord[0] = event.pos().x()
        self.coord[1] = event.pos().y()
        self.coord[2] = event.pos().x()
        self.coord[3] = event.pos().y()
        self.round_coord()
        self.update()
        self.released = False
        self.update_text_key('Mouse position',str(int(self.coord[2]/self.image_scale))+","+str(int(self.coord[3]/self.image_scale)))

    def mouseMoveEvent(self, event):
        #tracking mouse position for wheel change
        

        self.wheel_x = event.pos().x()/self.image_scale
        self.wheel_y = event.pos().y()/self.image_scale

        if len(self.label_lists) != len(self.coord_list):#no label is set
            return
        self.coord[2] = event.pos().x()
        self.coord[3] = event.pos().y()
        self.round_coord()
        self.update()
        self.update_text_key('Mouse position',str(int(self.coord[2]/self.image_scale))+","+str(int(self.coord[3]/self.image_scale)))


    def mouseReleaseEvent(self,event):
        self.x = QCursor.pos().x()
        self.y=  QCursor.pos().y()
        if len(self.label_lists) != len(self.coord_list):#no label is set
            return

        self.coord[2] = event.pos().x()
        self.coord[3] = event.pos().y()
        self.round_coord()
        self.update()
        x0,y0,x1,y1 = self.coord
        if x0>x1:
            tmp = x1
            x1 = x0
            x0 = tmp
        if y0>y1:
            tmp = y1
            y1 = y0
            y0 = tmp

        if x1!=x0 and y1!=y0:
            self.position_lists.append((x0,y0,x1,y1,self.image_scale,(self.pixmap().width(),self.pixmap().height())))
            self.coord_list.append([x0,y0,x1,y1])
            if int(self.num_class) > 1:
                self.window_status.append("opened")
                self.ppw = popupwindow(int(self.num_class),self.label_lists,self.window_status,int(self.x),int(self.y))
                self.ppw.show()
            else:
                self.label_lists.append(1)
        set_mouse_press_position(event.pos().x(),event.pos().y())
        #print(event.pos().x(),event.pos().y())
        self.released = True 


    def setImageScale(self,scale):
        #recover real bounding boxes
        self.image_scale = scale



    def scale(self,scale_rate):
        for coord in self.coord_list: 
            for i in range(4):
                coord[i] =coord[i]*scale_rate

            
    def undo(self):
        if len(self.window_status) > 0:
            #pop up window not closed 
            return
        if len(self.coord_list) > 0:
            del self.coord_list[-1]
            del self.position_lists[-1]
            del self.label_lists[-1]
           
            self.update()


    def get_bboxes(self):
        position_lists = self.position_lists.copy()
        label_lists = self.label_lists.copy()
       
        self.history_all.append((position_lists,label_lists))
        return self.position_lists,self.label_lists
    def previous(self):
   
        self.position_lists,self.label_lists= self.history_all[-1]
        for i in range(len(self.position_lists)):
                x,y,z,w,scale,_ = self.position_lists[i]
                self.coord_list.append([x/scale*self.image_scale,y/scale*self.image_scale,z/scale*self.image_scale,w/scale*self.image_scale]) 
        del(self.history_all[-1])
        self.update()
        #del(self.history_all[-1])



class my_QLabel_painter(my_QLabel):
    def __init__(self):
        super(my_QLabel_painter,self).__init__()
        self.setStyleSheet('QFrame {background-color:white;}')
        self.coord = [0,0,0,0]
        self.coord_list = []
        self.position_lists = []
        self.penRectangle = QtGui.QPen(QtCore.Qt.green)
        self.penRectangle.setWidth(1)
        self.released = True
        self.image_scale = 1
        self.qpoints = []
        self.last_pos = None
        self.current_pos = None
        self.lines = []
        self.qimage = None
        self.qimage_ori = None
        self.qimage_list = []
        self.counter = 0
        self.is_painting = True
        self.wheel_x = 0
        self.wheel_y = 0
        self.setMouseTracking(True)

    def setNumClasses(self,value):
        pass
    def set_bbx_color(self,value):
        pass
    def set_pen_width(self,value):
        self.penRectangle.setWidth(int(value))
    def setNumClasses(self,value):
        pass
    def set_qimage(self,qimage):
        del(self.qimage_list[:])
        qimg_ori = qimage.copy()
        self.qimage_ori = QPixmap.fromImage(qimg_ori) 
        self.qimage = qimage.copy()
        self.qimage_list.append(qimage)

    def get_qimage(self):
        return self.qimage
    def save_img(self,path,img_format="bmp"):
        self.pixmap().save(path,img_format)                #self.update()

    
    #def wheelEvent(self,event):
    #    super().wheelEvent(event)
    #    delta = event.angleDelta()
    #   print(delta)
    def get_wheel_ratio(self):
        width,height = self.pixmap().width(),self.pixmap().height()
        return self.wheel_x/width,self.wheel_y/height

    def round_coord(self):
        for i in range(len(self.coord)):
            self.coord[i] = int(self.coord[i])



    cc = 0
    def paintEvent(self, event):
        super().paintEvent(event)

        if self.qimage is not None and self.DRAWING_MODE is "PAINT":
            qp = QtGui.QPainter(self.qimage)
            #qp.begin(current_pix_map)
            br = QtGui.QBrush(QtGui.QColor(100, 10, 10, 40))  
            #qp.setBrush(br)   
            qp.setPen(self.penRectangle)
            #for qpoint in self.qpoints:
            #    qp.drawPoints(qpoint)
            for line in self.lines:
                qp.drawLine(line[0]/self.image_scale,line[1]/self.image_scale)
            #qp.drawLine(self.last_pos/self.image_scale,self.current_pos/self.image_scale)
            self.counter = self.counter + 1
            
            if len(self.lines)  > 0  and (self.is_painting is True) :
                current_pix_map = QPixmap.fromImage(self.qimage) 
                self.setPixmap(current_pix_map)
                #self.update()

        elif self.qimage is not None and self.DRAWING_MODE is "ERASE":
            qp = QtGui.QPainter(self.qimage)
            #br = QtGui.QBrush(QtGui.QColor(100, 10, 10, 40))  
            #qp.setPen(self.penRectangle)

            radius_eraser = 1 
            x,y = self.coord[2]/self.image_scale,self.coord[3]/self.image_scale
            x = x-radius_eraser
            y = y-radius_eraser
            for i in range(-10,10):
                for j in range(-10,10):
                    qp.drawTiledPixmap(x-i,y-j,radius_eraser,radius_eraser,self.qimage_ori,x-i,y-j)

            if self.is_painting is True :
                current_pix_map = QPixmap.fromImage(self.qimage) 
                self.setPixmap(current_pix_map)
            self.cc += 1


        del(self.lines[:])
        


    def clear_labels(self):
        del(self.coord_list[:])
        del(self.position_lists[:])


    def mousePressEvent(self, event):
        self.is_painting = True
        self.coord[0] = event.pos().x()
        self.coord[1] = event.pos().y()
        self.coord[2] = event.pos().x()
        self.coord[3] = event.pos().y()
        self.round_coord()
        self.qpoints.append(QPoint(event.pos().x(),event.pos().y()))
        self.last_pos = event.pos() 
        self.current_pos = event.pos()
        #self.lines.append((self.last_pos,self.current_pos))
        
        self.update()
        self.released = False

    def mouseMoveEvent(self, event):
        #tracking mouse position for wheel change
        self.wheel_x = event.pos().x()/self.image_scale
        self.wheel_y = event.pos().y()/self.image_scale
        if self.released is False:
            self.last_pos = self.current_pos
            self.coord[2] = event.pos().x()
            self.coord[3] = event.pos().y()
            self.current_pos = event.pos()
            self.round_coord()
            self.qpoints.append(QPoint(event.pos().x(),event.pos().y()))
            self.lines.append((self.last_pos,self.current_pos))
            self.update()
            '''
            if self.qimage is not None:
                qp = QtGui.QPainter(self.qimage)
                #qp.begin(current_pix_map)
                br = QtGui.QBrush(QtGui.QColor(100, 10, 10, 40))  
                #qp.setBrush(br)   
                qp.setPen(self.penRectangle)
                #for qpoint in self.qpoints:
                #    qp.drawPoints(qpoint)
                for line in self.lines:
                    qp.drawLine(line[0]/self.image_scale,line[1]/self.image_scale)
                self.counter = self.counter + 1
            '''

        


    def mouseReleaseEvent(self,event):
        self.coord[2] = event.pos().x()
        self.coord[3] = event.pos().y()
        self.round_coord()
        self.update()
        x0,y0,x1,y1 = self.coord
        if x1!=x0 and y1!=y0:
            self.position_lists.append((x0,y0,x1,y1,self.image_scale))
            self.coord_list.append([x0,y0,x1,y1])
        self.released = True 
        
        if self.qimage is not None:
            #only save copy when mouse is released to undo
            qimage = self.qimage.copy()
            self.qimage_list.append(qimage)

    def setImageScale(self,scale):
        #recover real bounding boxes
        self.image_scale = scale



    def scale(self,scale_rate):
        for coord in self.coord_list: 
            for i in range(4):
                coord[i] =coord[i]*scale_rate

        '''
        for line in self.lines:
            point0,point1 = line
            point0.setX(point0.x()*scale_rate)
            point0.setY(point0.y()*scale_rate)
            point1.setX(point1.x()*scale_rate)
            point1.setY(point1.y()*scale_rate)
       '''
            
    def undo(self):
        self.is_painting = False
        if len(self.coord_list) > 0:
            del(self.coord_list[-1])
            self.update()
        if len(self.qimage_list) > 1:
            #print(len(self.qimage_list))
            del(self.qimage_list[-1])
            self.qimage = self.qimage_list[-1].copy()
            current_pix_map = QPixmap.fromImage(self.qimage) 
            self.setPixmap(current_pix_map)
            #self.update()

    def is_labeled(self):
        return len(self.qimage_list) > 1

    def get_bboxes(self):
        return self.position_lists,[]

class popupinfo(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.setWindowTitle("Information")
        self.setGeometry(300,300,1000,500)


class popupwindow(QWidget):
    def __init__(self,num_class = 2,label_val = [],window_status=[],x=200,y=200,colum_number = 5):
        self.colum_number = colum_number
        self.num_class = num_class
        self.window_status = window_status
        QWidget.__init__(self)
        w_w,w_h = get_screen_size()
        self.setWindowTitle('Set the label')
        
        #print(x,y)
        g_x =int((num_class/5+1)*50*self.colum_number/5)
        self.setGeometry(x,y,500,g_x)
        #self.setGeometry(int(w_w/2),int(w_h/2),500,500)
        self.raise_()
        self.setWindowFlags(Qt.Window | Qt.CustomizeWindowHint | Qt.WindowStaysOnTopHint)
        self.label_dic = getLabelDic()
        self.set_layout()
        self.label_number = 0
        self.label_val = label_val
        

    def set_layout(self):
        self.setStyleSheet("background-color: lightyellow;")
        self.layout = QGridLayout()
        self.setLayout(self.layout)
        #menubar = QMenuBar()
        #actionFile = menubar.addMenu("File")
        #self.layout.addWidget(menubar, 0, 0,1,1)
        #actionFile.addAction("Select image folder")
        #self.button_frame = QFrame(self)
        #self.button_frame.setFrameShape(QFrame.StyledPanel)
        #self.button_frame.setFrameShadow(QFrame.Raised)
        #self.layout.addWidget(self.button_frame,1,0,30,1)


        '''
        self.imageLabel = my_QLabel_painter()
        #self.imageLabel.set_qpainter(self.qpixmap)
        self.imageLabel.setBackgroundRole(QPalette.Base)
        self.imageLabel.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        self.imageLabel.setScaledContents(True)
        self.scrollArea = my_QScrollArea()
        self.scrollArea.setBackgroundRole(QPalette.Dark)
        self.scrollArea.setWidget(self.imageLabel)
        '''


        #self.layout.addWidget(self.scrollArea,31,0,60,1)
        if self.num_class > 9:
            col_index = 0
            row_index = 1 
            self.text_info = QLabel("Input the class id or click the button")
            self.text_class = QLineEdit()
            self.layout.addWidget(self.text_info,0,0,1,int(self.colum_number*3/5))
            self.layout.addWidget(self.text_class,0,int(self.colum_number*3/5),1,self.colum_number-int(self.colum_number*3/5))
            self.text_class.returnPressed.connect(self.set_label_from_txt)
        else:
            col_index = 0
            row_index = 0

        column = self.colum_number 
        button_group = QButtonGroup()
        for i in range(self.num_class):
            #print(col_index,row_index)
            button = QRadioButton(self.label_dic[str(i+1)])
            button_group.addButton(button)
            button.toggled.connect(lambda state,arg0=(i+1):self.set_label(arg0))
            self.layout.addWidget(button,row_index,col_index,1,1)
            if (i+1) % column == 0 and i > 0:
                col_index = 0
                row_index = row_index + 1
            else:
                col_index = col_index+1
        
        
    def closeEvent(self,event):
        self.label_val.append(self.label_number)
        del(self.window_status[:])

    def set_label(self,label_number):
        self.label_number = label_number
        self.close()

    def set_label_from_txt(self):
        label_number = self.text_class.text()
        try:
            label_number = int(label_number)
        except:
            print("Please input valid number")
            self.text_class.setText("")
            return
        if label_number > self.num_class:
            self.text_class.setText("")
            return
        self.label_number = label_number
        self.close()
        


    def keyPressEvent(self, event):
        
        if event.key() == QtCore.Qt.Key_1:
            self.set_label(1)
        if event.key() == QtCore.Qt.Key_2:
            self.set_label(2)    
        if event.key() == QtCore.Qt.Key_3:
            self.set_label(3)
        if event.key() == QtCore.Qt.Key_4:
            self.set_label(4)
        if event.key() == QtCore.Qt.Key_5:
            self.set_label(5)
        if event.key() == QtCore.Qt.Key_6:
            self.set_label(6)
        if event.key() == QtCore.Qt.Key_7:
            self.set_label(7)
        if event.key() == QtCore.Qt.Key_8:
            self.set_label(8)
        if event.key() == QtCore.Qt.Key_9:
            self.set_label(9)
            
