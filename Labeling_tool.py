'''
    Copyright (C) <2019>  <natureofnature>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''
from PyQt5.QtCore import *
from PIL import Image,ImageDraw
from PyQt5 import QtCore,QtGui,QtWebEngineWidgets
from PyQt5.Qt import QCursor
from PyQt5.QtGui import QImage, QPainter, QPalette, QPixmap, QColor, QFont, QDoubleValidator, QFocusEvent
from Configure import getConfig,setConfig,getLastDialogue,setPath,getLabelDic
import imghdr
import threading
import numpy as np
from PIL import Image
import os
from PyQt5.QtWidgets import *
import sys
import time
import datetime
from shutil import copy, move,rmtree
from Paint_window import popupwindow,my_QLabel_painter,my_QLabel_rectangle
from global_variable import set_screen_size,set_mouse_press_position

Image.MAX_IMAGE_PIXELS = 1000000000
paint_bg = True

class my_QScrollArea(QScrollArea):
    def __init__(self,widget=None,key_press_mode="Normal"):
        super(my_QScrollArea,self).__init__()
        self.registered_widget = widget 
        self.key_press_mode = key_press_mode
        self.q_img = QPixmap("./configure_files/bg.jpg")
        self.setStyleSheet('QScrollArea { background: transparent; }')
        #if key_press_mode == "Normal":
            #self.setFocusPolicy(Qt.StrongFocus)
    
    def get_size(self):
        return self.width(),self.height()

    def resizeEvent(self,event):
        super().resizeEvent(event)
        if self.registered_widget is not None:
            self.registered_widget.resize(self.width(),self.height())

    def wheelEvent(self,event):
        #override,do nothing
        pass

    def keyPressEvent(self,event):
        #prevent text bar from scrolling
        if self.key_press_mode== "Normal":
            super().keyPressEvent(event)
        else:
            pass

    def paintEvent(self,event):
        super().paintEvent(event)
        global paint_bg
        if paint_bg is True:
            painter = QPainter(self.viewport())
            painter.drawPixmap(self.rect(), self.q_img)
class html_window(QtWebEngineWidgets.QWebEngineView):
    def __init__(self):
        super(html_window, self).__init__()
        dirpath = os.getcwd()
        Url = "file:////"+os.path.join(dirpath,"configure_files/documents/index.html").replace('\\','/')
        self.setWindowIcon(QtGui.QIcon("./configure_files/ninja-simple-512.ico"))
        self.setWindowTitle('Help')
        #self.load(QtCore.QUrl("file:////"+os.path.join(dirpath,"configure_files/documents/index.html")))
        self.load(QtCore.QUrl(Url))

class my_frame(QMdiArea):
    def __init__(self):
        super(my_frame,self).__init__()

    def paintEvent(self,event):
        super().paintEvent(event)
        painter = QPainter(self.viewport())
        painter.drawPixmap(self.rect(), QPixmap("./configure_files/bg.jpg"))




class Window(QWidget):
    



    mouse_shapes={"HAND":Qt.PointingHandCursor,"Cross":Qt.CrossCursor,"Forbid":Qt.ForbiddenCursor}

    def set_menu_style(self,menu):
        menu.setStyleSheet("""
        QMenuBar::item {
            spacing: 3px;
            padding: 2px 10px;
            background-color: rgb(55,75,95);
            color: rgb(255,255,255);
            border-radius: 5px;
        }
        QMenuBar::item:selected {
            background-color: rgb(244,164,96);
        }
        QMenuBar::item:pressed {
            background: rgb(128,0,0);
        }
        QMenu {
            background-color: rgb(49,49,49);
            color: rgb(255,255,255);
            border: 1px solid ;
        }

        QMenu::item::selected {
            background-color: rgb(30,30,30);

        }
        """)

    def get_attributes(self):
        dic,_ = getConfig()
        self.window_width = int(dic['width'])
        self.window_height= int(dic['height'])
        self.label_mode = dic['label_mode']
        self.verify_bounding_box = dic['verify_bounding_box']
        self.num_class = dic['number_classes']
        self.gdic = dic
    

        
        




        

        
    def __init__(self,screen):

        self.label_mode = "bounding_box_mode"
        self.log_file = "./log.txt"
        #self.label_mode = "painting_mode"
        self.last_wheel_pos = [0,0]

        screen_size = screen.size()
        set_screen_size(screen_size.width(),screen_size.height())
        self.get_attributes()
        QWidget.__init__(self)
        self.setCursor(self.mouse_shapes["Cross"])
        self.setWindowIcon(QtGui.QIcon("./configure_files/ninja-simple-512.ico"))
        self.setWindowTitle('Label_tool_V2.1.0_by_LWZ')
        self.layout = QGridLayout()
        self.setLayout(self.layout)
        menubar = QMenuBar()
        self.set_menu_style(menubar)
        self.layout.addWidget(menubar, 0, 0,1,1)
        actionFile = menubar.addMenu("File")
        #(file_in_origin_folder,file_in_target)
        #if OK, file_in_target is path/name
        #if NG, file_in_target is path
        self.history= []


        self.key_to_display={}
        self.key_to_display['Folder'] = ""
        self.key_to_display['Total file'] = 0
        self.key_to_display['Index'] = 0
        self.key_to_display['Image name'] = ""
        self.key_to_display['Path saving labeled image'] = "Default path"
        self.key_to_display['Path saving unlabeled image'] = "Default path"
        self.key_to_display['Move_mode'] = 'Copy only' 
        self.key_to_display['Number of class'] = self.num_class
        self.key_to_display['Mouse position'] = "0,0"
        




        #Part 1
        actionFile.addAction("Select image folder").triggered.connect(self.open_folder)
        actionFile.addAction("Saving image to ...").triggered.connect(self.set_path_Root)
        #actionFile.addAction("Saving unlabeld image to...").triggered.connect(self.set_path_OK)
        actionFile.addSeparator()
        actionFile.addAction("Quit").triggered.connect(self.Quit)
        self.set_menu_style(actionFile)


        #Part 2
        self.next_action = menubar.addAction("Next (N)")
        self.next_action.triggered.connect(self.next_image)

        #Part 3
        menubar.addAction("Previous (P)").triggered.connect(self.previous)
        menubar.addAction("Undo (Z)").triggered.connect(self.undo)

        #part 3
        option_menu = menubar.addMenu("Options")
        
        '''------------number of class settings--------------------'''
        self.class_menu = option_menu.addMenu("Number of classes = "+self.num_class)
        #for i in range(10):
        #    self.class_menu.addAction(str(i+1)).triggered.connect(lambda state,arg0=(i+1):self.setClassNumber(arg0))
        self.text_class = QLineEdit()
        self.text_class.setText(self.num_class)
        #self.text_class.setValidator(QDoubleValidator(0,100,2))
        self.text_class.setStyleSheet("background-color:white ; color: black;")
        self.text_action = QWidgetAction(None)
        self.text_action.setDefaultWidget(self.text_class)
        self.class_menu.addAction(self.text_action)
        self.text_class.returnPressed.connect(self.setClassNumber)
        #self.text_class.focusOutEvent.connect(self.setClassNumber)
        

        '''-----------color settings------------------------------'''
        color_setting_menu = option_menu.addMenu("Color settings")
        bbx_color_menu = color_setting_menu.addMenu("BBx_color")
        ruler_color_menu = color_setting_menu.addMenu("Ruler_color")
        for i in ["red","yellow","white","blue","green","black"]:
            bbx_color_menu.addAction(i).triggered.connect(lambda state,arg0=i:self.set_bbx_color(arg0))
            ruler_color_menu.addAction(i).triggered.connect(lambda state,arg0=i:self.set_ruler_color(arg0))
        panel_color_menu = color_setting_menu.addMenu("Image panel color")
        for i in ["lightyellow","black","gray","white","lightgreen"]:
            panel_color_menu.addAction(i).triggered.connect(lambda state,arg0=i:self.set_panel_color(arg0))
        text_panel_color_menu = color_setting_menu.addMenu("Text panel color")
        for i in ["lightyellow","black","gray","white","lightgreen"]:
            text_panel_color_menu.addAction(i).triggered.connect(lambda state,arg0=i:self.set_text_panel_color(arg0))


        '''----------pen width settings-------------------------'''
        if self.label_mode== "painting_mode":
            self.menu_bar_painting = QMenuBar()
            self.set_menu_style(self.menu_bar_painting)
            self.layout.addWidget(self.menu_bar_painting,1,0,1,1)
            self.pen_width_menu = self.menu_bar_painting.addMenu("Pen width")
            self.erase_menu = self.menu_bar_painting.addAction("Paint").triggered.connect(lambda state, arg0="PAINT":self.set_paint_mode(arg0))
            self.erase_menu = self.menu_bar_painting.addAction("Eraser").triggered.connect(lambda state, arg0="ERASE":self.set_paint_mode(arg0))
            #for i in [5,10,20,30,40,50,60,70,80,100]:
            #       pen_width_menu.addAction(str(i)).triggered.connect(lambda satte,arg0=i:self.imageLabel.set_pen_width(arg0))
            self.width_slider = QSlider(Qt.Horizontal)
            self.width_slider.setFocusPolicy(Qt.StrongFocus)
            self.width_slider.setTickPosition(QSlider.TicksBothSides)
            self.width_slider.setTickInterval(10)
            self.width_slider.setStyleSheet("background-color:white ; color: black;")
            self.width_slider.setSingleStep(1)
            self.width_slider.setMinimum(1)
            self.width_slider.setMaximum(100)
            self.slider_action=QWidgetAction(None)
            self.slider_action.setDefaultWidget(self.width_slider)
            self.width_slider.valueChanged.connect(self.set_pen_width)
            self.pen_width_menu.addAction(self.slider_action)
        

        
        '''----------move mode settings--------------------------'''
        move_mode_menu = option_menu.addMenu("Move mode")
        move_mode_menu.addAction("Keep original images").triggered.connect(self.set_move_false)
        move_mode_menu.addAction("Move original images").triggered.connect(self.set_move_true)

        verify_result_menu = option_menu.addMenu("verify_bounding_box")
        for i in ["yes","no"]:
            verify_result_menu.addAction(i).triggered.connect(lambda state,arg0=i:self.set_verify_bounding_box(arg0))

        switch_info = "" 
        if self.label_mode== "painting_mode":
            switch_info = "Switch to bounding box mode"
        else:
            switch_info = "Switch to painting mode"
        switch_mode = option_menu.addAction(switch_info).triggered.connect(self.switch_between_bbx_painnting_mode)


        #part 4
        self.hide_info_action = menubar.addAction("Display text panel")
        self.hide_info_action.triggered.connect(self.change_panel_appearance)
        self.is_hide_info = True 
        #part 5
        self.htm_win = None
        menubar.addAction("Help").triggered.connect(self.help)
        self.set_menu_style(option_menu)



        self.setGeometry(300,300,self.window_width,self.window_height)
        self.center()
        self.set_layout()
        #controller
        self.scale_rate = 1.25
        self.scale = 1
        self.image_lists = None
        self.image_name = None
        path_dic = getLastDialogue()
        self.NG_path = "Not specified"
        self.OK_path = "Not specified"
        self.last_root_dir = path_dic['last_save_folder_Root']
        self.move_mode = False
        self.last_image = "./configure_files/endbg.png"
        self.first_image="./configure_files/beginbg.png"
        self.wheel_angle = 0
        self.font = QFont()

    def center(self):
        # geometry of the main window
        qr = self.frameGeometry()
        display_monitor = 0
        center = QDesktopWidget().screenGeometry(display_monitor).center()

        #monitor = QDesktopWidget().screenGeometry(display_monitor)
        #self.move(monitor.left(), monitor.top())
        qr.moveCenter(center)
        self.move(qr.topLeft())
        #self.showFullScreen()

    def set_pen_width(self):
        self.imageLabel.set_pen_width(self.width_slider.value())
        #print("value changed to ",self.width_slider.value())

    def set_paint_mode(self,value):
        self.imageLabel.set_paint_mode(value)


    def switch_between_bbx_painnting_mode(self):
        if self.label_mode == "painting_mode":
            setConfig(self.gdic,"label_mode","bounding_box_mode")
        else:
            setConfig(self.gdic,"label_mode","painting_mode")
        global paint_bg 
        paint_bg = True
        qApp.exit(-15123123)
        self.close()


        
    def change_panel_appearance(self):
        if self.is_hide_info == False:
            self.splitter.setSizes([1000,0])
            self.is_hide_info = True
            self.hide_info_action.setText("Display text panel")
        else:
            self.splitter.setSizes([760,240])
            self.is_hide_info = False
            self.hide_info_action.setText("Hide text panel")

    def set_verify_bounding_box(self,value):
        self.verify_bounding_box = value
        

    def set_panel_color(self,value):
        self.setStyleSheet("background-color: "+value+";")
        self.image_panel_bg_color = value
        if value == 'black':
            self.image_panel_fg_color = 'white'
        else:
            self.image_panel_fg_color = 'black'
        self.imageLabel.setStyleSheet("QLabel { color : "+self.image_panel_fg_color+"; }")


    def set_text_panel_color(self,value):
        self.text_box.setStyleSheet("background-color: "+value+";")
        if value == 'black':
            self.text_box.setTextColor(QColor(255,255,255))
        else:
            self.text_box.setTextColor(QColor(0,0,0))


    def set_bbx_color(self,value):
        self.imageLabel.set_bbx_color(value)
    def set_ruler_color(self,value):
        self.imageLabel.set_ruler_color(value)

    def setClassNumber(self,value= "-1"):
        if value == "-1":
            class_number = self.text_class.text()
            try:
                class_number = int(class_number)
            except:
                self.printf("You should input a valid number")
                return
            if class_number >100:
                self.printf("Large class number")
                return
            
            class_number = str(class_number)
            self.imageLabel.setNumClasses(class_number)
            self.num_class = class_number
            self.update_text_key('Number of class',self.num_class)
            self.class_menu.setTitle("Number of classes = "+str(self.num_class))
        else:
            self.imageLabel.setNumClasses(value)
            self.num_class = value
            self.update_text_key('Number of class',value)
            self.class_menu.setTitle("Number of classes = "+str(self.num_class))

        setConfig(self.gdic,"number_classes",self.num_class)

        
    def help(self):
        self.htm_win = html_window()
        self.htm_win.setGeometry(600, 150, 1000, 1000)
        self.htm_win.show()




    def set_move_false(self):
        self.move_mode = False
        self.update_text_key('Move_mode','Copy only')
        #self.printf("Original files will not be removed")

    def set_move_true(self):
        self.move_mode = True
        self.update_text_key('Move_mode','Move original image')
        #self.printf("Original files will be removed")

        
    def open_folder(self):
        #reset background
        try:
            self.imageLabel.clear_all()
            del(self.history[:])
        except:
            self.printf("Warning: exception when clearing history")
            pass
        #image = QImage(self.first_image)
        #qpixmap = QPixmap.fromImage(image)
        self.imageLabel.clear()
        self.imageLabel.setStyleSheet("background-color: "+self.image_panel_bg_color);

        path_dic = getLastDialogue()
        last_image_dir = path_dic['last_source_folder']
        if not os.path.exists(last_image_dir) or not os.path.isdir(last_image_dir):
            last_image_dir = "./"
        
        path = QFileDialog.getExistingDirectory(self, 'Select directory',directory = last_image_dir)
        while path is None or len(path) == 0:
            path = QFileDialog.getExistingDirectory(self, 'Select directory',directory = last_image_dir)
        setPath('last_source_folder',path)
        #self.printf("Selected folder ("+path+")")
        self.update_text_key('Folder', path)
        
        self.image_lists = [os.path.join(path,i) for i in os.listdir(path)]
        
        #self.printf("There are "+str(len(self.image_lists))+" files in the selected folder")
        self.update_text_key('Total file', len(self.image_lists))
        self.image_index = 0
        self.image_name = None
        self.next_action.setText("Display Image")
        self.splitter.setSizes([760,240])
        self.is_hide_info = False
        self.hide_info_action.setText("Hide text panel")
        self.next_image()

    def set_path_Root(self):
        path_dic = getLastDialogue()
        self.last_root_dir = path_dic['last_save_folder_Root']
        if not os.path.exists(self.last_root_dir) or not os.path.isdir(self.last_root_dir):
                self.last_root_dir = "./"
        path = QFileDialog.getExistingDirectory(self, 'Select root directory',directory = self.last_root_dir)
        while path is None or len(path) == 0:
            path = QFileDialog.getExistingDirectory(self, 'Select root directory',directory = self.last_root_dir)
        setPath('last_save_folder_Root',path)
        self.last_root_dir = path
        now_time = datetime.datetime.now()
        time_string = str(now_time.year)+"-"+str(now_time.month)+"-"+str(now_time.day)+"-"+str(now_time.hour)

        self.NG_path = os.path.join(self.last_root_dir,"Labeled_image_"+time_string)
        self.OK_path = os.path.join(self.last_root_dir,"Unlabeld_image_"+time_string)
        if not os.path.exists(self.NG_path):
                os.makedirs(self.NG_path)
        if not os.path.exists(self.OK_path):
                os.makedirs(self.OK_path)
        self.update_text_key('Path saving labeled image',self.NG_path)
        self.update_text_key('Path saving unlabeled image',self.OK_path)

    def set_path_NG(self):
        path_dic = getLastDialogue()
        last_image_dir = path_dic['last_save_folder_NG']
        if not os.path.exists(last_image_dir) or not os.path.isdir(last_image_dir):
            last_image_dir = "./"
        
        path = QFileDialog.getExistingDirectory(self, 'Select directory',directory = last_image_dir)
        while path is None or len(path) == 0:
            path = QFileDialog.getExistingDirectory(self, 'Select directory',directory = last_image_dir)
        setPath('last_save_folder_NG',path)
        self.NG_path = path
        #self.printf("NG path is set to: "+self.NG_path)
        self.update_text_key('Path saving labeled image',self.NG_path)
        
    def set_path_OK(self):
        path_dic = getLastDialogue()
        last_image_dir = path_dic['last_save_folder_OK']
        if not os.path.exists(last_image_dir) or not os.path.isdir(last_image_dir):
            last_image_dir = "./"
        
        path = QFileDialog.getExistingDirectory(self, 'Select directory',directory = last_image_dir)
        while path is None or len(path) == 0:
            path = QFileDialog.getExistingDirectory(self, 'Select directory',directory = last_image_dir)
        setPath('last_save_folder_OK',path)
        self.OK_path = path
        #self.printf("OK path is set to: "+self.OK_path)
        self.update_text_key('Path saving unlabeled image',self.OK_path)
     

    
    def save_result(self):
        if self.image_name is None:
            return
        if self.OK_path is None or self.NG_path is None:
            self.printf("Please select OK/NG paths first")
        base_name = os.path.basename(self.image_name)
        base_name,_ = os.path.splitext(base_name) 

        target_folder_NG = os.path.join(self.NG_path,base_name)
        target_folder_OK = os.path.join(self.OK_path,base_name)

        
        bbxes,label_lists = self.imageLabel.get_bboxes()
        #self.imageLabel.save_img("/dev/shm/m1/123.bmp")
        #print("--->",self.image_name)

        if self.label_mode == "painting_mode":
            im = Image.open(self.image_name)
            im_copy = im.copy()
            status = self.imageLabel.is_labeled()
            if status is True: #has mask
                if not os.path.exists(target_folder_NG):
                    os.makedirs(target_folder_NG)
                #print(os.path.join(target_folder_NG,base_name+".bmp"))
                self.imageLabel.save_img(os.path.join(target_folder_NG,base_name+"_labeled.bmp"))
                if self.move_mode is False:
                    copy(self.image_name,os.path.join(target_folder_NG,os.path.basename(self.image_name)))
                else:
                    move(self.image_name,os.path.join(target_folder_NG,os.path.basename(self.image_name)))
                im1 = Image.open(os.path.join(target_folder_NG,base_name+"_labeled.bmp"))
                im2 = Image.open(os.path.join(target_folder_NG,os.path.basename(self.image_name))).convert('RGB')
                array1 = np.array(im1)
                array2 = np.array(im2)
                array3 = array1-array2
                im3 = Image.fromarray(array3)
                im3.save(os.path.join(target_folder_NG,base_name+"_mask.bmp"))
            else:
                if self.move_mode is False:
                    copy(self.image_name,os.path.join(self.OK_path,os.path.basename(self.image_name)))
                else:
                    move(self.image_name,os.path.join(self.OK_path,os.path.basename(self.image_name)))


        else:
            if len(bbxes) > 0:
                if not os.path.exists(target_folder_NG):
                    os.makedirs(target_folder_NG)
                #NG images
                try:
                    if self.verify_bounding_box == 'yes':
                        im = Image.open(self.image_name)
                        im = im.convert('RGB')
                       
                        draw = ImageDraw.Draw(im)
                    with open(os.path.join(target_folder_NG,base_name+".txt"),'w') as f:
                        line_index = 0
                        for box in bbxes:
                            x0,y0,x1,y1,ratio,(c_w,c_h) = box 
                            x0 = int(x0/ratio)
                            y0 = int(y0/ratio)
                            x1 = int(x1/ratio)
                            y1 = int(y1/ratio)
                            x0 = max(0,x0)
                            y0 = max(0,y0)
                            x1 = min(x1,c_w)
                            y1 = min(y1,c_h)
                            lab = label_lists[line_index]
                            if self.verify_bounding_box == 'yes':
                                draw.rectangle(((x0,y0),(x1,y1)),outline='yellow')
                            if line_index > 0:
                                f.write('\n')
                            f.write(str(x0)+","+str(y0)+","+str(x1)+","+str(y1)+","+str(lab))
                            line_index = line_index + 1
                            if self.verify_bounding_box == 'yes':
                                im_cropped = im.crop((x0,y0,x1,y1))
                                im_cropped.save(os.path.join(target_folder_NG,base_name+"_rect_"+str(line_index)+".jpg"))
                                

                except Exception as e:
                    self.printf(str(e))
                    print(e)
                if self.verify_bounding_box == 'yes':
                    im.save(os.path.join(target_folder_NG,base_name+"_rect_whole.jpg"))
                
                try:
                    if self.move_mode is False:
                        copy(self.image_name,os.path.join(target_folder_NG,os.path.basename(self.image_name)))
                    else:
                        move(self.image_name,os.path.join(target_folder_NG,os.path.basename(self.image_name)))

                    image_n,target_folder,target_n= self.image_name,target_folder_NG,os.path.join(target_folder_NG,os.path.basename(self.image_name))
                    #("type",index,original_name,new_name,new_folder)
                    self.history.append(('NG',self.image_index-1,image_n,target_n,target_folder))
                except Exception as e:
                    self.printf(str(e))

            else:
                #if not os.path.exists(target_folder_OK):
                #    os.makedirs(target_folder_OK)
                #print(self.image_name)
                try:
                    if self.move_mode is False:
                        copy(self.image_name,os.path.join(self.OK_path,os.path.basename(self.image_name)))
                    else:
                        move(self.image_name,os.path.join(self.OK_path,os.path.basename(self.image_name)))
                    image_n,target_folder,target_n= self.image_name,self.OK_path,os.path.join(self.OK_path,os.path.basename(self.image_name))
                    self.history.append(('OK',self.image_index-1,image_n,target_n,target_folder))
                except Exception as e:
                    self.printf(str(e))
                #OK images

            

    def meet_last_image(self):
        self.image_name = None
        self.printf("No remaining images to be labeled, current index: "+str(self.image_index))
        self.imageLabel.clear()
        self.font.setPointSize(30)
        self.font.setBold(True)
        self.font.setWeight(75)
        self.imageLabel.setFont(self.font)
        self.imageLabel.setStyleSheet("background-color: "+self.image_panel_bg_color);
        self.imageLabel.setStyleSheet("QLabel { color : "+self.image_panel_fg_color+"; }")
        self.imageLabel.setText("THIS IS THE END")

    def reset_font(self):
        self.font.setPointSize(10)
        self.font.setBold(False)
        self.font.setWeight(10)
        self.imageLabel.setFont(self.font)

    def next_image(self):
        self.scrollArea.setFocus()
        if self.image_lists is None:
            self.printf("Please select image folder first")
            return
        if self.imageLabel is None:
            self.printf("Please select labeling mode first")
            return 
        if not os.path.exists(self.last_root_dir):
                self.printf("Please set root dir for saving")
                return

        global paint_bg
        paint_bg = False
        #-------------------------------------#
        # check and create forlder for saving

        now_time = datetime.datetime.now()
        time_string = str(now_time.year)+"-"+str(now_time.month)+"-"+str(now_time.day)+"-"+str(now_time.hour)
        self.NG_path = os.path.join(self.last_root_dir,"Labeled_image_"+time_string)
        if os.path.exists(self.NG_path): 
            pass
        else:
            os.makedirs(self.NG_path)

          
        now_time = datetime.datetime.now()
        time_string = str(now_time.year)+"-"+str(now_time.month)+"-"+str(now_time.day)+"-"+str(now_time.hour)
        self.OK_path = os.path.join(self.last_root_dir,"Unlabeld_image_"+time_string)
        if os.path.exists(self.OK_path):
                pass  
        else:  
            os.makedirs(self.OK_path)
           
        self.update_text_key('Path saving labeled image',self.NG_path)
        self.update_text_key('Path saving unlabeled image',self.OK_path)
        #-------------------------------------#
        self.save_result()
        self.imageLabel.clear()

        self.imageLabel.clear_labels()
        if len(self.image_lists) < 1 or self.image_index >=len(self.image_lists):
            self.meet_last_image()
            return 
        #skip files that are not images
        while self.image_index < len(self.image_lists):
            image_name = self.image_lists[self.image_index] 
            if os.path.isdir(image_name) or (not imghdr.what(image_name) in ['jpg','jpeg', 'bmp', 'png', 'tiff']):
                self.image_name = None
            else:
                break
            self.image_index = self.image_index + 1
            self.update_text_key('Index',self.image_index + 1)
        if self.image_index >=len(self.image_lists):
            self.meet_last_image()
            return
        self.reset_font()
        #self.printf("Processing: [index = "+str(self.image_index+1)+"] "+image_name)
        self.update_text_key('Index',self.image_index + 1)
        self.update_text_key('Image name',image_name)

        image = QImage(image_name)
        qpixmap = QPixmap.fromImage(image)
        self.imageLabel.setPixmap(qpixmap)
        self.imageLabel.set_qimage(image)

        scroll_width, scroll_height = self.scrollArea.get_size()
        image_width, image_height = qpixmap.width(),qpixmap.height()
        if image_width == 0 or image_height == 0:
            return
        ratio = min(scroll_width/image_width,scroll_height/image_height)
        self.scale = ratio
        self.imageLabel.setImageScale(self.scale)
        image_width = int(image_width*ratio)
        image_height = int(image_height*ratio)
        self.imageLabel.resize(image_width,image_height)
        self.image_name = image_name
        self.image_index = self.image_index + 1
        self.next_action.setText("Next (N)")


        
    def Quit(sel):
        sys.exit(0)

    def keyPressEvent(self, event):
        if self.imageLabel is None or self.imageLabel.pixmap() is None:
            return
        if event.key() == QtCore.Qt.Key_Equal:
            self.scale = self.scale * self.scale_rate
            self.imageLabel.setImageScale(self.scale)
            self.imageLabel.resize(self.imageLabel.pixmap().size()*self.scale)
            #set coord
            self.imageLabel.scale(self.scale_rate)
        elif event.key() == QtCore.Qt.Key_Minus:
            self.scale = self.scale / self.scale_rate
            self.imageLabel.setImageScale(self.scale)
            self.imageLabel.resize(self.imageLabel.pixmap().size()*self.scale)
            self.imageLabel.scale(1/self.scale_rate)
        elif event.key()==(Qt.Key_Control and Qt.Key_Z):
            self.imageLabel.undo()
        elif event.key()==(Qt.Key_N):
            self.printf("go to next")
            self.next_image() 
        elif event.key()==(Qt.Key_P):
            self.previous()

    def wheelEvent(self,event):
        if self.imageLabel is None or self.imageLabel.pixmap() is None:
            return
        super().wheelEvent(event)
        delta = event.angleDelta()
        #self.wheel_angle = self.wheel_angle + delta.y()
        hz_bar = self.scrollArea.horizontalScrollBar()
        vt_bar = self.scrollArea.verticalScrollBar()
        #print(hz_bar.value())
        #print(vt_bar.value())
        #print(event.x())
        #print(event.y())

        if delta.y() > 0:
            self.scale = self.scale * self.scale_rate
            self.imageLabel.setImageScale(self.scale)
            self.imageLabel.resize(self.imageLabel.pixmap().size()*self.scale)
            #set coord
            self.imageLabel.scale(self.scale_rate)
        else:
            self.scale = self.scale / self.scale_rate
            self.imageLabel.setImageScale(self.scale)
            self.imageLabel.resize(self.imageLabel.pixmap().size()*self.scale)
            self.imageLabel.scale(1/self.scale_rate)
        width = self.scrollArea.frameGeometry().width()
        height = self.scrollArea.frameGeometry().height()
        #ratio_x = event.x()/width
        #ratio_y = event.y()/height
        ratio_x,ratio_y = self.imageLabel.get_wheel_ratio()
        hz_bar.setValue(int(hz_bar.maximum()*ratio_x))
        vt_bar.setValue(int(vt_bar.maximum()*ratio_y))


        

    def closeEvent(self,event):
        if self.htm_win is not None:
            self.htm_win.close()
            del(self.htm_win)
            self.htm_win = None
        super().closeEvent(event)
        


       
    def set_layout(self):
        self.setStyleSheet("background-color: black;")
        self.image_panel_bg_color = "black"
        self.image_panel_fg_color = 'white'
        self.splitter = QSplitter(Qt.Vertical)
        
        #---------------up-------------------#
        if self.label_mode == "painting_mode":
            self.imageLabel = my_QLabel_painter()
            #self.imageLabel.set_qpainter(self.qpixmap)
        else:
            self.imageLabel = my_QLabel_rectangle(self.update_text_key)

        self.imageLabel.setBackgroundRole(QPalette.Base)
        self.imageLabel.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        self.imageLabel.setScaledContents(True)

        self.scrollArea = my_QScrollArea()
        self.scrollArea.setBackgroundRole(QPalette.Dark)
        self.scrollArea.setWidget(self.imageLabel)
 

        #---------------down----------------#
        self.text_box= QTextEdit() 
        self.text_box.setStyleSheet("background-color: lightyellow;")
        self.text_box.setReadOnly(True)
        #self.text_box.setMaximumSize(10000000,150)
        self.scrollArea_info = my_QScrollArea(self.text_box,"ignore_key")
        self.scrollArea_info.setBackgroundRole(QPalette.Dark)
        self.scrollArea_info.setWidget(self.text_box)

    
        self.splitter.addWidget(self.scrollArea)
        self.splitter.addWidget(self.scrollArea_info)
        self.splitter.setSizes([1000,0])
        self.is_hide_info = True
        if self.label_mode== "painting_mode":
            self.layout.addWidget(self.splitter,2,0,89,1)
        else:
            self.layout.addWidget(self.splitter,1,0,90,1)


    def printf(self,info):
        self.text_box.clear()
        self.text_box.append(info)
        with open(self.log_file,'a') as f:
            f.write(info+"\n")

    def update_displaying_text(self):
        info = ""
        for i,j in self.key_to_display.items():
            info = info+"["+str(i)+"]: "+str(j)+"\n"

        self.text_box.clear()
        self.text_box.append(info)

    def update_text_key(self,key,value):
        self.key_to_display[key] = value
        with open(self.log_file,'a') as f:
            if key != "Mouse position":
                f.write(str(key)+":->"+str(value)+"\n")
    
        self.update_displaying_text()

    def undo(self):
        self.imageLabel.undo()

    def previous(self):
        self.imageLabel.clear_labels()
        
        if len(self.history) <= 0:
            self.printf("This is already the begining")
            return
        
        image_type, index, image_name,target_name, target_folder = self.history[-1]
        self.image_index = index
        
        if image_name != self.image_lists[self.image_index]:
            self.printf("Inconsistent version")
        if image_type == "NG":
            if not os.path.exists(image_name):
                move(target_name,image_name)
            rmtree(target_folder)
        else:
            if not os.path.exists(image_name):
                move(target_name,image_name)
            else:
                os.remove(target_name)


        self.reset_font()
             
        image = QImage(image_name)
        qpixmap = QPixmap.fromImage(image)
        self.imageLabel.setPixmap(qpixmap)
        self.imageLabel.set_qimage(image)
        self.image_name = image_name
        
        scroll_width, scroll_height = self.scrollArea.get_size()
        image_width, image_height = qpixmap.width(),qpixmap.height()
        if image_width == 0 or image_height == 0:
            return
        ratio = min(scroll_width/image_width,scroll_height/image_height)
        self.scale = ratio
        self.imageLabel.setImageScale(self.scale)
        image_width = int(image_width*ratio)
        image_height = int(image_height*ratio)
        self.imageLabel.resize(image_width,image_height)
        self.image_name = image_name
        self.image_index = self.image_index + 1
        self.next_action.setText("Next (N)")



        del(self.history[-1])
        
        self.imageLabel.previous()

        self.update_text_key('Index',self.image_index)
        self.update_text_key('Image name',image_name)




#app = QApplication(sys.argv)
#scr = app.primaryScreen()
#screen = Window(scr)
#screen.show()
#sys.exit(app.exec_())
